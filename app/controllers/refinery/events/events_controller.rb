module Refinery
  module Events
    class EventsController < ::ApplicationController

      before_filter :find_all_events
      before_filter :find_page

      def init_shared
        @month_hash = {
            '1' => 'JAN',
            '2' => 'FEB',
            '3' => 'MAR',
            '4' => 'APR',
            '5' => 'MAY',
            '6' => 'JUN',
            '7' => 'JUL',
            '8' => 'AUG',
            '9' => 'SEPT',
            '10' => 'OCT',
            '11' => 'NOV',
            '12' => 'DEC'
        }

        time = Time.new

        @month = params[:m]
        @month ||= time.month.to_s

        @year = params[:y]
        @year ||= time.year

        @years = Event.select(:when_start).map { |e| e.when_start.year }.uniq.sort.reverse
      end

      def index

        init_shared()

        @events = Event.where('MONTH(when_start) = ? and year(when_start) = ?', @month, @year).order(:when_start)

        # you can use meta fields from your model instead (e.g. browser_title)
        # by swapping @page for @event in the line below:
        present(@page)
      end


      def show

        init_shared()

        @event = Event.find(params[:id])

        # you can use meta fields from your model instead (e.g. browser_title)
        # by swapping @page for @event in the line below:
        present(@page)
      end

      protected

      def find_all_events
        @events = Event.order('position ASC')
      end

      def find_page
        @page = ::Refinery::Page.where(:link_url => "/events").first
      end
    end
  end
end
