module Refinery
  module Events
    class Event < Refinery::Core::BaseModel
      self.table_name = 'refinery_events'

      attr_accessible :what, :when_start, :when_time_start, :when_end, :when_time_end, :where, :details, :position

      acts_as_indexed :fields => [:what, :where, :details]

      validates :what, :presence => true, :uniqueness => true
    end
  end
end
