
FactoryGirl.define do
  factory :event, :class => Refinery::Events::Event do
    sequence(:what) { |n| "refinery#{n}" }
  end
end

