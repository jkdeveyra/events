require 'spec_helper'

module Refinery
  module Events
    describe Event do
      describe "validations" do
        subject do
          FactoryGirl.create(:event,
          :what => "Refinery CMS")
        end

        it { should be_valid }
        its(:errors) { should be_empty }
        its(:what) { should == "Refinery CMS" }
      end
    end
  end
end
